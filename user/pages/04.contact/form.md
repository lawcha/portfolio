---
title: Contact
icon: fa-edit
body_classes: right-wave
form:
    name: contact
    classes: row
    fields:
        name:
            outerclasses: col-md-6
            classes: 'form-control input-lg'
            label: Name
            placeholder: 'Enter your name'
            autocomplete: 'on'
            type: text
            validate:
                required: true
        email:
            outerclasses: col-md-6
            classes: 'form-control input-lg'
            label: Email
            placeholder: 'Enter your email address'
            type: email
            validate:
                required: true
        message:
            outerclasses: col-md-12
            classes: 'form-control input-lg'
            label: Message
            placeholder: 'Enter your message'
            type: textarea
            rows: '5'
            validate:
                required: true
    buttons:
        submit:
            type: submit
            value: Submit
            outerclasses: text-center
            classes: 'btn btn-primary btn-lg'
    process:
        save:
            fileprefix: contact-
            dateformat: Ymd-His-u
            extension: txt
            body: '{% include ''forms/data.txt.twig'' %}'
        email:
            subject: '[Site Contact Form] {{ form.value.name|e }}'
            body: '{% include ''forms/data.html.twig'' %}'
        message: 'Thank you for getting in touch!'
        display: thankyou
---

# Contact
If you have any question, don't hesitate to contact me with the form below.