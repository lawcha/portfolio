---
title: 'Institut Feeling'
taxonomy:
    tag:
        - web
        - wordpress
        - showcase
media_order: 'institut-feeling.png,image_2021-01-27_091228.png'
body_classes: right-wave
---

![](image_2021-01-27_091228.png?classes=right%20mw-100%20shadow&resize=400,225)
"Institut Feeling" is a beauty institute. They wanted a website to inform their clients about services, special offers and to provide general informations about the institute.

<div class="clearfix"></div>
### Visual identity
The main choise of colour is the purple. It is associated with spirituality, peace and vitality. Is is also the main colour of the existing logo. The serif and thin typography gives a luxury and elegrant feeling.

<div class="clearfix"></div>
[center]<a href="https://institut-feeling.ch" target="_blank" class="btn btn-primary mt-4" data-aos="zoom-in">Visit the website</a>[/center]