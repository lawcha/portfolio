---
title: 'Collection 1900'
media_order: 'carousel-0.jpeg,7840.jpg,logo.png,construction.png,hero.png'
taxonomy:
    tag:
        - web
        - vuejs
        - showcase
body_classes: right-wave
---

![Assemblage d'un stylo](hero.png?classes=right%20mw-100%20shadow&resize=400,225)
The website "Collection 1900" presents a set of hand-made pens. These products are made by an independant carpenter in his tiny workshop next to his home.

He wanted a website to present all his creations to stores and allowing them to contact him. 
<div class="clearfix"></div>
### Visual identity
![Assemblage d'un stylo](construction.png?classes=right%20mw-100%20shadow&resize=400,225)
The design of the website has been thought to show the elegant and traditional design of the products. A simple modern look, combining dark and light colours, accent the well-made aspect of the products. Moreover, the gold yellow is chose as secondary colour. It gives a more sophisticated vision of the products.
<div class="clearfix"></div>
[center]<a href="https://collection1900.g-pac.ch" target="_blank" class="btn btn-primary mt-4" data-aos="zoom-in">Visit the website</a>[/center]