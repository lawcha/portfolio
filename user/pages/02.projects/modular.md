---
title: Projects
body_classes: right-wave
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _professional
            - _school
icon: fa-folder-open
---

