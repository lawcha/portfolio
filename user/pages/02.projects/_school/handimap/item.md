---
title: HandiMap
media_order: 'background.png,image_2021-01-24_210618.png,image_2021-01-24_221836.png'
published: true
taxonomy:
    tag:
        - mobile
        - ios
        - android
        - flutter
        - iot
body_classes: right-wave
---

HandiMap promotes smart parking for disabled people. In other words, allows people with reduced mobility to easily find a parking space using an application especially dedicated to them and a connected pole.

In their daily life people with reduce mobility (PRM) have difficulties linked to their handicap. One of these is to move. We live in a world where the roads are full of traffic. So it is often hard to find the best way to a parking spot. Moreover disabled people need a specific one which is adapted to their handicap.

#### How does it works ?
![How does it work](image_2021-01-24_221836.png?classes=right%20mw-100&resize=400,250)
The mobile application **HandiMap** allows to persons with reduced mobility to easily find a parking spot. It guides the user to the desired parking spot and shows if the spot is free or not. How ? A pole is placed on the spot and detect if there is a car parked.<br>
Moreover, in an effort of limiting the fraud, the pole has a light which shows if the parked vehicle is authorized to park here. Indeed people poorly look to persons who are parked on these parking spots by being not authorized to. The light is a way to increase the culpability felt by unauthorized people. The light is also useful for the authorities to control the spots.

The application is deveopped for Android and iOS smartphones.

#### China Hardware Innovation Camp (CHIC)
This project take place in the "CHIC" program. It is an interdisciplinary project. The development team is composed of people coming from computer science, mechanical engineering, electronic engineering and business management. The goal is to produce a connected device from scratch which is here, a pole. The final goal is to realize a prototype of the product in China. Unfortunately, due to the situation with the COVID, the trip is not done for this edition. My project concerns the IT part, particularly the development of the mobile application.

### Features
[widget load="/widgets/_handimap-features" /]

### Find your favourite parking spot
<div markdown=1 data-aos="fade-left" data-aos-duration="600">![Preview of the app](app_overview.png?classes=right%20mw-100&resize=600,300)</div>
The application provides an intuitive and similar interface like Google Maps. Search for the city you want to go to or just navigate on the map to find a free parking spot. Once you've found it, let the navigation system (GPS) guides you. Finally, you just have to confirm when you arrive to let know the other users.

<div class="clearfix"></div>
### A secured solution
<div markdown=1 data-aos="fade-right" data-aos-duration="600">![](image_2021-01-25_195523.png?classes=left%20mw-100&resize=300,300)</div>
Our solution prevent unauthorised people to access the application. Indeed it is mandatory to register with your parking permit the first time you try the application. Our scanning algorithm will validate your permit and only the expiration date is stored in our system.

<div class="clearfix"></div>
[center]<a href="https://chi.camp/projects/team-fr-2019-2020/" target="_blank" class="btn btn-primary" data-aos="zoom-in">More on this project</a>[/center]