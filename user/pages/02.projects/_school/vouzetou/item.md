---
title: Vouzêtou
media_order: 'vouzetou_banner.jpeg,docs_VideoHumanTech.mp4,image_2021-01-25_114720.png'
body_classes: right-wave
taxonomy:
    tag:
        - mobile
        - ios
        - android
        - geolocation
        - flutter
widget:
    areas:
        -
            location: default
            widgets:
                -
                    load: /widgets/_vouzetou
                    enabled: true
---

Haven't you ever got lost in those gigantic festivals or fairs? The huge crowd and the omnipresent animation make losing friends and family easy. Call them? With all this noise? What if finding them would be more than a simple modality? **Vouzêtou** is a project that aims to remedy this problem.
<div markdown="1" class="row">
<div markdown="1" class="col-md-9 col-xs-12">
Vouzêtou is a mobile application for Android and iOS. His goal is to locate a group of friends in a festival to avoid getting lost. Each friend install the application on their device and join the same group.

The main features of the application are:
- Locate his friends or family in real-time in a festival
- Invite friends to a group by sharing an invitation code
- Join, create or delete a group
- Find quickly where is a person on the map


#### Improvements
The application is yet at a prototype state. Android support is not fully operational concerning the background tracking. Moreover, a major update would be to add an overlay image of the festivals. Indeed, currently it just track the users on a map.
</div>
<div markdown="1" class="col-md-3 col-6 m-auto">
![](image_2021-01-25_114720.png?classes=w-100)
</div>
</div>
### Features
[widget load="/widgets/_vouzetou" /]


### Trailer of the application
<div markdown="1" class="text-center">
![](docs_VideoHumanTech.mp4?classes=w-responsive%20rounded)
</div>