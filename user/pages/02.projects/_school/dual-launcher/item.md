---
title: 'Dual Launcher'
media_order: dual_launcher-min.png
taxonomy:
    tag:
        - mobile
        - android
body_classes: right-wave
---

Work is hard. And even more nowaday where the distraction is always with us: Our smartphone. For people, hours are lost because of social media, notifications and so on that disturb our concentration.

Our solution allow Android users to separate completely their work from their hobbies. How ? By providing a specific launcher for application concerning only the work.