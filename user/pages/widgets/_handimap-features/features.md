---
title: 'Handimap features'
body_classes: modular
features:
    -
        icon: 'fas fa-sync-alt'
        text: 'Realtime availabilities'
    -
        icon: 'fas fa-bullhorn'
        text: 'Fraud alert'
    -
        icon: 'fas fa-lock'
        text: Security
    -
        icon: 'fas fa-smile'
        text: Intuitive
---

The pole capture the status of a parking spot and inform you in real time of their availability.

A led is integrated to the pole to disuade unauthorized people to park.

The access is restricted to people with reduce mobility only. Moreover, we respect your private data as much as we can.

We care about the understandability of our system. We help you step by step to be sure you understand how to use the application.