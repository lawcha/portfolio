---
title: 'Vouzetou features'
body_classes: modular
features:
    -
        text: 'Real-time tracking'
        icon: 'fas fa-map-marker-alt'
    -
        text: Secure
        icon: 'fas fa-lock'
    -
        text: Cross-platform
        icon: 'fas fa-mobile-alt'
---

Each group member position is tracked in real-time, allowing you to know where everyone is at any moment.

Your data are deleted once the event ends or when you quit a group.

The application is available for Android and iOS.