---
title: Home
body_classes: 'title-center title-h1h2 h-overflow left-wave'
bodywrapper_classes: h-100
visible: false
heading0: 'Hi, I''m'
heading1: Laurent
subtitle: 'Web design & mobile development'
---

[center]
<a href="./projects" class="btn btn-lg btn-secondary" data-aos="zoom-in" data-aos-duration="1000" data-aos-duration="600">See my projects</a>
[/center]