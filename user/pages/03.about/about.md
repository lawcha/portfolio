---
title: 'About me'
body_classes: right-wave
icon: fa-user
pagetitle: 'Laurent Chassot'
footer:
    -
        text: 'See my work'
        icon: 'fas fa-folder-open'
        link: ./portfolio
skills:
    -
        icon: 'fas fa-globe'
        text: 'Web development'
    -
        icon: 'fas fa-mobile-alt'
        text: 'Android & iOS development'
    -
        icon: 'fas fa-paint-brush'
        text: 'UX Design'
links:
    -
        icon: 'fab fa-linkedin-in'
        name: 'Linked In'
        link: 'https://www.linkedin.com/in/laurent-chassot/'
    -
        icon: 'fab fa-github'
        name: GitHub
        link: 'https://github.com/lawcha/'
    -
        icon: 'fab fa-gitlab'
        name: GitLab
        link: 'https://gitlab.com/lawcha/'
    -
        icon: 'fab fa-medium'
        name: Medium
        link: 'https://laurent-chassot.medium.com/'
timeline:
    -
        date: '2020 - Today'
        title: 'Master Student'
        description: 'HES-SO Master, Computer Sciences'
    -
        date: '2017 - 2020'
        title: 'Bachelor Student'
        description: 'School of Engieneering and Architecture of Fribourg, Computer Science'
    -
        date: '2013 - 2017'
        title: 'CFC Student'
        description: 'Vocational school of Fribourg, Computer Science'
thumbnail-link: 'https://example.com'
---

After a 3-year at the School of Engineering in Fribourg where I received my Bachelor's degree, I am currently continuing my studies with a Master's degree. Passionate about technology and music, I am curious about everything and I try to learn new things as soon as I can. 