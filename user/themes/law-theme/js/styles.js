$('.navbar-toggler').click(function () {
  // Change the icon
  $('.animated-icon').toggleClass('open');
  // Disable the scroll on body while the menu is opened
  if ($('.navbar-toggler').hasClass('collapsed')) {
    $('body').css('overflow', 'hidden');
  } else {
    $('body').css('overflow', '');
  }
});