$( document ).ready(function() {
  gsap.from(".main-title-rectangle", {duration: 1.5, y: -100, opacity: 0, ease: "slow", scale: 0.95});
  gsap.from(".main-subtitle", {duration: 2, opacity: 0, ease: "slow", scale: 0.95, delay: 0.5});
});