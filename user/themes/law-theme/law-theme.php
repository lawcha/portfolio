<?php
namespace Grav\Theme;

use Grav\Common\Grav;
use Grav\Common\Theme;

class LawTheme extends Theme {
  public static function getSubscribedEvents() {
    return [
      'onThemeInitialized'    => ['onThemeInitialized', 0],
      'onTwigLoader'          => ['onTwigLoader', 0],
      'onTwigInitialized'     => ['onTwigInitialized', 0],
    ];
  }

  public function onThemeInitialized()
  {

  }
  // Add images to twig template paths to allow inclusion of SVG files
  public function onTwigLoader() {
    $theme_paths = Grav::instance()['locator']->findResources('theme://images');
    foreach($theme_paths as $images_path) {
      $this->grav['twig']->addPath($images_path, 'images');
    }
  }

  public function onTwigInitialized() {
  }
}
